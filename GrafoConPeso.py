# graph is in adjacent list representation
from queue import PriorityQueue
import queue

graph = {'A': ['B', 'C'],
         'B': ['D', 'E'],
         'C': ['F', 'G'],
         'D': ['B'],
         'E': ['B','H'],
         'F': ['C'],
         'G': ['C'],
         'H': ['E']
         }

graph3 = {'1': ['4', '3','2'],
         '2': ['1', '3'],
         '3': ['1', '4'],
         '4': ['1','3','5']  , '5': ['4']     
         }

graph2 = {"1":{"2":1,"3":2},
        "2":{"3":3},
        "3":{"4":4},
         "4":{"1":5}

    }

graph4 = {'1': ['2', '3','4'],
         '2': ['1', '3'],
         '3': ['1', '2','3'],
         '4': ['1','3']     
         }

graph5 = {"0":{"1":4,"2":3},
        "1":{"2":5,"3":2},
        "2":{"3":7},
         "3":{"4":2},
        "4":{"0":4,"1":4,"5":6},

    }


def bfs(graph, start, end):
    # maintain a queue of paths
    queue = []
    # push the first path into the queue
    queue.append([start])
    while queue:
        # get the first path from the queue
        print('queue = ')
        print(queue)
        path = queue.pop(0)
       # print(type(path))
        # get the last node from the path
        node = path[-1]
       # print(type(path))
        print('Nodo = '+node)
        # path found
        if node == end:
            return path
        # enumerate all adjacent nodes, construct a new path and push it into the queue
        for adjacent in graph.get(node, []):
            print('Adj '+adjacent)
            new_path = list(path)
            print(new_path)
            new_path.append(adjacent)
            print(new_path)
            queue.append(new_path)


def bfs_search(graph_to_search, start, end):
    queue = [[start]]
    visited = set()

    while queue:
        # Gets the first path in the queue
        path = queue.pop(0)

        # Gets the last node in the path
        vertex = path[-1]

        # Checks if we got to the end
        if vertex == end:
            return path
        # We check if the current node is already in the visited nodes set in order not to recheck it
        elif vertex not in visited:
            # enumerate all adjacent nodes, construct a new path and push it into the queue
            for current_neighbour in graph_to_search.get(vertex, []):
                new_path = list(path)
                new_path.append(current_neighbour)
                queue.append(new_path)

            # Mark the vertex as visited
            visited.add(vertex)            
###opcion intentar con una lista


#print(list(customers.queue))

def ucs_(graph2, start, end):
    # maintain a queue of paths
    cola = PriorityQueue()
    # push the first path into the queue
    cola.put((0,[start]))
    while (cola.qsize()>0) :
        # get the first path from the queue
        print('queue = ')
        print(list(cola.queue))
        path = cola.get()
        print('path = ')
        print(path)
        # get the last node from the path
        node = path[-1]
        node=node[-1]
       # print(type(path))
        print('Nodo= ')
        print(node)
       # print(end)
        # path found
        if node == end:
            return path
        # enumerate all adjacent nodes, construct a new path and push it into the queue
        for adjacent in graph2.get(node, []):
            print("adjacent "+adjacent)
            print(node, " -> ", adjacent, " edge weight: ", graph2[node][adjacent])
            aux=path[0]
            print("aux= ",aux)
            new_path = list(path[-1])
            print(new_path)
            new_path.append(adjacent)
            print(new_path)
            cola.put((graph2[node][adjacent]+aux,new_path))
 

print(bfs(graph4, '1', '4'))
print('_______________________')
print('_______________________')
print('_______________________')
var=ucs_(graph5, '0', '3')
print("El mejor camino es",var[1],"con un costo de: ",var[0])

"""
print("short: ")
print(bfs_search(graph3, '1', '5'))"""
"""
def bfs(graph, start, end):
    # maintain a queue of paths
    queue = []
    # push the first path into the queue
    queue.append([start])
    while queue:
        # get the first path from the queue
        print('queue = ')
        print(queue)
        path = queue.pop(0)
       # print(type(path))
        # get the last node from the path
        node = path[-1]
       # print(type(path))
        print('Nodo = '+node)
        # path found
        if node == end:
            return path
        # enumerate all adjacent nodes, construct a new path and push it into the queue
        for adjacent in graph.get(node, []):
            print('Adj '+adjacent)
            new_path = list(path)
            print(new_path)
            new_path.append(adjacent)
            print(new_path)
            queue.append(new_path)





def bfs_search_cola(graph, start, end):
    # maintain a queue of paths
    cola = queue.Queue();
    # push the first path into the queue
    cola.put([start])
    while (cola.qsize()>0) :
        # get the first path from the queue
        print('queue = ')
        print(cola)
        path = cola.get()
       # print(type(path))
        # get the last node from the path
        node = path[-1]
       # print(type(path))
        print('Nodo = '+node)
        # path found
        if node == end:
            return path
        # enumerate all adjacent nodes, construct a new path and push it into the queue
        for adjacent in graph.get(node, []):
            print('Adj '+adjacent)
            
            new_path = list(path)
            print(new_path)
            new_path.append(adjacent)
            print(new_path)
            cola.put(new_path)   """
